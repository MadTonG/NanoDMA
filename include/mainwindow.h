#ifndef UNKNOWNDMA_MAINWINDOW_H
#define UNKNOWNDMA_MAINWINDOW_H

#include <Windows.h>
#include <string>

struct SDL_Window;
class ID3D11Device;
class ID3D11DeviceContext;
class IDXGISwapChain;
class ID3D11RenderTargetView;
struct ID3D11ShaderResourceView;

typedef struct tkvMapResource {
    ID3D11ShaderResourceView* texture;
    int width;
    int height;
}tkvMapResource;


class MainWindow {
public:
    MainWindow();

    ~MainWindow();

    bool setupWindow(const std::string &title);

    bool show();

// [imgui sections]
private:
//    int loadMapFiles(const std::string& file_path);
    void showCombatOverlay(bool& show);

private:
    void processSDLEvent(bool &done);
    void updateWidgets();
    bool loadGameMapFromFile(std::string filename, ID3D11ShaderResourceView** srv, int& width, int& height);

    // GUI Sections
private:
    void showMainMenuBar();


private:
    // Forward declarations of helper functions
    bool CreateDeviceD3D(HWND hWnd);
    void CleanupDeviceD3D();
    void CreateRenderTarget();
    void CleanupRenderTarget();

    // Data
    SDL_Window *window_ = nullptr;
    ID3D11Device *g_pd3dDevice = nullptr;
    ID3D11DeviceContext *g_pd3dDeviceContext = nullptr;
    IDXGISwapChain *g_pSwapChain = nullptr;
    ID3D11RenderTargetView *g_mainRenderTargetView = nullptr;
    tkvMapResource current_map_;

private:
    bool show_app_main_menu_bar_ = true;
    bool show_combat_overlay_ = true;
    bool show_tool_debug_log_ = false;
    bool show_stack_tool_ = false;
    bool show_demo_window_ = false;
    bool show_about_window_ = false;

    void updateEFTData();
};


#endif //UNKNOWNDMA_MAINWINDOW_H
