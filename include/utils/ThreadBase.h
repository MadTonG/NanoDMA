//
// Created by 成都 on 2023/9/10.
//

#ifndef UNKNOWNDMA_THREADBASE_H
#define UNKNOWNDMA_THREADBASE_H



#include <thread>
#include <atomic>
#include <condition_variable>
#include <mutex>

class ThreadBase {
    enum class ThreadState:int {
        Stoped = 0,
        Running = 1,
        Paused = 2
    };
public:
    ThreadBase() {}
    virtual ~ThreadBase() { stop(); }

    void start();
    void pause();
    void resume();
    void stop();

    int currentState();

private:
    void handle();

protected:
    virtual void initialize() = 0;
    virtual void run() = 0;


private:
    ThreadState thread_state_ = ThreadState::Stoped;
    std::shared_ptr<std::thread> thread_ = nullptr;
    std::mutex mutex_;
    std::condition_variable condition_var_;
    std::atomic<bool> thread_pause_flag_ = false;
    std::atomic<bool> thread_stop_flag_ = true;
};


#endif //UNKNOWNDMA_THREADBASE_H
