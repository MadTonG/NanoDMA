//
// Created by 成都 on 2023/9/10.
//

#include "utils/ThreadBase.h"
void ThreadBase::start() {
    if (thread_ == nullptr) {
        thread_ = std::make_shared<std::thread>(&ThreadBase::handle, this);
        if (thread_ != nullptr) {
            thread_stop_flag_ = false;
            thread_pause_flag_ = false;
            thread_state_ = ThreadState::Running;
        }
    }

}

void ThreadBase::pause() {
    if (thread_ != nullptr)
    {
        if (thread_state_ == ThreadState::Running)
        {
            thread_pause_flag_ = true;
            thread_state_ = ThreadState::Paused;
        }
    }
}

void ThreadBase::resume() {
    if (thread_ != nullptr)
    {
        if (thread_state_ == ThreadState::Paused)
        {
            thread_pause_flag_ = false;
            condition_var_.notify_all();
            thread_state_ = ThreadState::Running;
        }
    }
}

void ThreadBase::stop() {
    if(thread_ != nullptr) {
        thread_stop_flag_ = true;
        thread_pause_flag_ = false;

        condition_var_.notify_all();

        if (thread_->joinable()) {
            thread_->join();
        }

        thread_.reset();
        if (thread_ == nullptr) {
            thread_state_ = ThreadState::Stoped;
        }
    }
}

int ThreadBase::currentState() {
    return static_cast<int>(thread_state_);
}

// this function work in other thread
void ThreadBase::handle() {

    initialize();

    while (!thread_stop_flag_) {
        try {
            run();
        } catch (std::exception &e) {
            break;
        }

        if (thread_pause_flag_) {
            std::unique_lock<std::mutex> locker(mutex_);
            if (thread_pause_flag_) {
                condition_var_.wait(locker);
            }
            locker.unlock();
        }


    }
    thread_pause_flag_ = false;
    thread_stop_flag_ = true;
}
