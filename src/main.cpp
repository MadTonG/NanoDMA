#include "mainwindow.h"
#include <thread>
#include <atomic>
#include <chrono>
#include <iostream>
#include "settings.h"


void dma_loop(bool & running)
{
}

int main(int argc, char** argv)
{
    Settings s;
    auto &connect = s.dma_connect;
    std::thread dma_thread([&connect]{
        static int i = 0;
        while (connect) {
            ++i;
            if (i > 10) {
                connect = false;
                break;
            }
            std::cout << i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        std::cout << "thread end." << std::endl;
    });
    dma_thread.detach();
    if (dma_thread.joinable())
        dma_thread.join();

    MainWindow w;
    if (w.setupWindow({"UnknownDMA <username>"})) {
        w.show();
    }
    else {
        return -1;
    }

    return 0;
}