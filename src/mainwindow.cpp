#include "mainwindow.h"
#include "imgui.h"
#include "imgui/imgui_impl_sdl2.h"
#include "imgui/imgui_impl_dx11.h"
#include <d3d11.h>
#include <d3d11shader.h>
#include <stdio.h>
#include "SDL2/SDL.h"
#include "SDL2/SDL_syswm.h"
#include <spdlog/spdlog.h>
#define STB_IMAGE_IMPLEMENTATION
#include "utils/stb_image.h"
#include "settings.h"
#pragma comment ( lib, "D3D11.lib")

MainWindow::MainWindow() {
}

MainWindow::~MainWindow() {
    // Cleanup
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    CleanupDeviceD3D();
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

bool MainWindow::setupWindow(const std::string &title) {
    // Setup SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
        spdlog::error("SDL init failed: {}", SDL_GetError());
        return false;
    }

#ifdef SDL_HINT_IME_SHOW_UI
    SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
#endif

    // Setup window
    SDL_WindowFlags window_flags = (SDL_WindowFlags) (SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    window_ = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    SDL_GetWindowWMInfo(window_, &wmInfo);
    HWND hwnd = (HWND) wmInfo.info.win.window;

    // Initialize Direct3D
    if (!CreateDeviceD3D(hwnd)) {
        CleanupDeviceD3D();
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void) io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForD3D(window_);
    ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);

    //* Load tarkov map files
    int width = 0;
    int height = 0;
    std::string filename = "../assets/hg.jpg";
    ID3D11ShaderResourceView* texture = nullptr;
    if (loadGameMapFromFile(filename, &texture, width, height)) {
        current_map_.texture = texture;
        current_map_.width = width;
        current_map_.height = height;
    }
    else {
        spdlog::error("loadGameMapFromFile filed.");
        return false;
    }
    return true;
}

bool MainWindow::show() {
    // Our state
    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // Main loop
    bool done = false;
    while (!done) {

        // 1. 处理SDL事件 (inputs, window resize, etc.)
        processSDLEvent(done);

        // 2. 初始化 frame
        ImGui_ImplDX11_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();

        // 3. 读取EFT数据
        updateEFTData();

        // 4. 更新用户界面
        updateWidgets();


        /*
         * 窗口绘制代码结束
         */

        // Rendering
        ImGui::Render();
        const float clear_color_with_alpha[4] = {clear_color.x * clear_color.w, clear_color.y * clear_color.w,
                                                 clear_color.z * clear_color.w, clear_color.w};
        g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, nullptr);
        g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, clear_color_with_alpha);
        ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

        g_pSwapChain->Present(1, 0); // Present with vsync
        //g_pSwapChain->Present(0, 0); // Present without vsync

    }

    return true;
}

bool MainWindow::CreateDeviceD3D(HWND hWnd) {
    // Setup swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 2;
    sd.BufferDesc.Width = 0;
    sd.BufferDesc.Height = 0;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    UINT createDeviceFlags = 0;
    //createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevelArray[2] = {D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0,};
    if (D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createDeviceFlags, featureLevelArray,
                                      2, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &featureLevel,
                                      &g_pd3dDeviceContext) != S_OK)
        return false;

    CreateRenderTarget();
    return true;
}

void MainWindow::CleanupDeviceD3D() {
    CleanupRenderTarget();
    if (g_pSwapChain) {
        g_pSwapChain->Release();
        g_pSwapChain = nullptr;
    }
    if (g_pd3dDeviceContext) {
        g_pd3dDeviceContext->Release();
        g_pd3dDeviceContext = nullptr;
    }
    if (g_pd3dDevice) {
        g_pd3dDevice->Release();
        g_pd3dDevice = nullptr;
    }
}

void MainWindow::CreateRenderTarget() {
    ID3D11Texture2D *pBackBuffer;
    g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
    g_pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, &g_mainRenderTargetView);
    pBackBuffer->Release();
}

void MainWindow::CleanupRenderTarget() {
    if (g_mainRenderTargetView) {
        g_mainRenderTargetView->Release();
        g_mainRenderTargetView = nullptr;
    }
}

//-----------------------------------------------------------------------------
// [SECTION] Example App: Simple overlay / ShowExampleAppSimpleOverlay()
//-----------------------------------------------------------------------------
// Demonstrate creating a simple static window with no decoration
// + a context-menu to choose which corner of the screen to use.
void MainWindow::showCombatOverlay(bool &open) {
        static int location = 0;
        ImGuiIO& io = ImGui::GetIO();
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
        if (location >= 0)
        {
            const float PAD = 10.0f;
            const ImGuiViewport* viewport = ImGui::GetMainViewport();
            ImVec2 work_pos = viewport->WorkPos; // Use work area to avoid menu-bar/task-bar, if any!
            ImVec2 work_size = viewport->WorkSize;
            ImVec2 window_pos, window_pos_pivot;
            window_pos.x = (location & 1) ? (work_pos.x + work_size.x - PAD) : (work_pos.x + PAD);
            window_pos.y = (location & 2) ? (work_pos.y + work_size.y - PAD) : (work_pos.y + PAD);
            window_pos_pivot.x = (location & 1) ? 1.0f : 0.0f;
            window_pos_pivot.y = (location & 2) ? 1.0f : 0.0f;
            ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
            window_flags |= ImGuiWindowFlags_NoMove;
        }
        else if (location == -2)
        {
            // Center window
            ImGui::SetNextWindowPos(ImGui::GetMainViewport()->GetCenter(), ImGuiCond_Always, ImVec2(0.5f, 0.5f));
            window_flags |= ImGuiWindowFlags_NoMove;
        }
        ImGui::SetNextWindowBgAlpha(0.35f); // Transparent background
        if (ImGui::Begin("Example: Simple overlay", &open, window_flags))
        {
//            IMGUI_DEMO_MARKER("Examples/Simple Overlay");
            ImGui::Text("Simple overlay\n" "(right-click to change position)");
            ImGui::Separator();
            ImGui::Text("%.1f FPS", io.Framerate);
            ImGui::Separator();
            if (ImGui::IsMousePosValid())
                ImGui::Text("Mouse Position: (%.1f,%.1f)", io.MousePos.x, io.MousePos.y);
            else
                ImGui::Text("Mouse Position: <invalid>");
            if (ImGui::BeginPopupContextWindow())
            {
                if (ImGui::MenuItem("Custom",       NULL, location == -1)) location = -1;
                if (ImGui::MenuItem("Center",       NULL, location == -2)) location = -2;
                if (ImGui::MenuItem("Top-left",     NULL, location == 0)) location = 0;
                if (ImGui::MenuItem("Top-right",    NULL, location == 1)) location = 1;
                if (ImGui::MenuItem("Bottom-left",  NULL, location == 2)) location = 2;
                if (ImGui::MenuItem("Bottom-right", NULL, location == 3)) location = 3;
                if (open && ImGui::MenuItem("Close")) open = false;
                ImGui::EndPopup();
            }
        }
        ImGui::End();

}

bool MainWindow::loadGameMapFromFile(std::string filename, ID3D11ShaderResourceView** srv, int& width, int& height) {
    // Load form disk into a raw RGBA buffer
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load(filename.c_str(), &image_width, &image_height, nullptr, 4);
    if (nullptr == image_data) {
        spdlog::error("stbi_load filed");
        return false;
    }

    // Create texture
    D3D11_TEXTURE2D_DESC t2d_desc;
    ZeroMemory(&t2d_desc, sizeof(D3D11_TEXTURE2D_DESC));
    t2d_desc.Width = image_width;
    t2d_desc.Height = image_height;
    t2d_desc.MipLevels = 1;
    t2d_desc.ArraySize = 1;
    t2d_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    t2d_desc.SampleDesc.Count = 1;
    t2d_desc.Usage = D3D11_USAGE_DEFAULT;
    t2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    t2d_desc.CPUAccessFlags = 0;

    ID3D11Texture2D *texture2d = nullptr;
    D3D11_SUBRESOURCE_DATA sub_resource;
    sub_resource.pSysMem = image_data;
    sub_resource.SysMemPitch = t2d_desc.Width * 4;
    sub_resource.SysMemSlicePitch = 0;
    g_pd3dDevice->CreateTexture2D(&t2d_desc, &sub_resource, &texture2d);

    // Create texture view
    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
    ZeroMemory(&srv_desc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
    srv_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srv_desc.Texture2D.MipLevels = t2d_desc.MipLevels;
    srv_desc.Texture2D.MostDetailedMip = 0;
    g_pd3dDevice->CreateShaderResourceView(texture2d, &srv_desc, srv);
    texture2d->Release();

    width = image_width;
    height = image_height;

    return true;
}

void MainWindow::showMainMenuBar() {
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("Tools"))
        {
            ImGui::MenuItem("Demo Window", nullptr, &show_demo_window_);
            ImGui::SeparatorText("Tools");
            ImGui::MenuItem("Debug Tool", nullptr, &show_tool_debug_log_);
            ImGui::MenuItem("Stack Tool", nullptr, &show_stack_tool_);
            ImGui::Separator();
            ImGui::MenuItem("About", nullptr, &show_about_window_);
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void MainWindow::processSDLEvent(bool &done) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        ImGui_ImplSDL2_ProcessEvent(&event);
        if (event.type == SDL_QUIT)
            done = true;
        if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE &&
            event.window.windowID == SDL_GetWindowID(window_))
            done = true;
        if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED &&
            event.window.windowID == SDL_GetWindowID(window_)) {
            // Release all outstanding references to the swap chain's buffers before resizing.
            CleanupRenderTarget();
            g_pSwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
            CreateRenderTarget();
        }
    }
}

#include <vmmdll.h>

void MainWindow::updateEFTData() {
}

/*
 * TODO 窗口绘制代码开始
 */
void MainWindow::updateWidgets() {


    // [Section] 背景地图
    ImGui::GetBackgroundDrawList()->AddImage((ImTextureID)current_map_.texture,
                                             ImVec2(0.0f, 0.0f),
                                             ImVec2(current_map_.width, current_map_.height));

    // [Section] 战斗信息浮窗
    if (show_combat_overlay_) showCombatOverlay(show_combat_overlay_);
    if (show_app_main_menu_bar_) showMainMenuBar();
    if (show_tool_debug_log_) ImGui::ShowDebugLogWindow(&show_tool_debug_log_);
    if (show_stack_tool_) ImGui::ShowStackToolWindow(&show_stack_tool_);
    if (show_demo_window_) ImGui::ShowDemoWindow(&show_demo_window_);
    if (show_about_window_) ImGui::ShowAboutWindow(&show_about_window_);

    // [Section] 物品Loot

    // [Section] 游戏设置

    // [Section]
}

